﻿using HumanResources.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HumanResurces.Infra.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<CargoFuncao> CargoFuncao { get; set; }
        public DbSet<Colaborador> Colaborador { get; set; }
        public DbSet<Contrato> Contrato { get; set; }
    }
}
