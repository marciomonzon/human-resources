﻿using HumanResources.Domain.Entities;
using Xunit;

namespace HumanResources.UnitTests.Domain
{
    public class ContratoTests
    {
        private Contrato _contrato;

        [Theory]
        [InlineData("PJ")]
        [InlineData("CLT")]
        public void Should_Not_Throw_Exception_Tipo_Contrato_Via_Constructor(string tipoContrato)
        {
            // Arrange
            string dataInicio = "20/08/2022";

            // Act
            var exception = Record.Exception(() => _contrato = new Contrato(tipoContrato, dataInicio));
            
            // Assert
            Assert.Null(exception);
        }

        [Fact]
        public void Should_Not_Throw_Exception_DataDeInicio_Via_Constructor()
        {
            // Arrange
            string tipoContrato = "PJ";
            string dataInicio = "20/08/2022";

            // Act
            var exception = Record.Exception(() => _contrato = new Contrato(tipoContrato, dataInicio));

            // Assert
            Assert.Null(exception);
        }

        [Theory]
        [InlineData("PJA")]
        [InlineData("CLTA")]
        public void Should_Throw_Exception_Tipo_Contrato_Via_Constructor(string tipoContrato)
        {
            // Arrange
            string dataInicio = "20/08/2022";

            // Act
            var exception = Record.Exception(() => _contrato = new Contrato(tipoContrato, dataInicio));

            // Assert
            Assert.NotNull(exception);
        }

        [Fact]
        public void Should_Throw_Exception_DataDeInicio_Via_Constructor()
        {
            // Arrange
            string tipoContrato = "PJ";
            string dataInicio = "20/15/2022";

            // Act
            var exception = Record.Exception(() => _contrato = new Contrato(tipoContrato, dataInicio));

            // Assert
            Assert.NotNull(exception);
        }
    }
}
